# Mathematica notebook on the ct and cg relation for different masses of Higgs particle.

Please have a look at the [ct_cg.pdf](ct_cg.pdf) or [ct_cg.nb](ct_cg.nb).
